Sprite Sheet
============

HINT: If you're looking for a sprite *atlas* generator, take a look at [sprpack](https://gitlab.com/bztsrc/spratlas).

Little command line tool to mass convert sprite sheets from batch files. It can be used to automatically create a new format
sprite sheet asset from an old format sprite sheet asset, or to merge an asset with separate files (one file per animation)
into one single sheet. With specifying output suffixes, it can split a single sheet into separate images as well.

```
sprsheet by bzt Copyright (C) 2022 MIT license
 https://gitlab.com/bztsrc/sprsheet

./sprsheet [-s <w>,<h>] [-f <png>] [-r <png>] [-p <png>] [-t] <csv>|- <output png> <input #1> [input #2...]

 -s <w>,<h>     specify the output image's size (otherwise calculated from CSV)
 -f <png>       force colors to be the ones on the parameter image
 -r <png>       replace colors using image rows (first row "to", others "from")
 -p <png>       define palette variations and de-colorize
 -t             make the output a truecolor image
 -e             save output even if it's empty
 <csv>          CSV file that describes the areas to be used, one row per frame
 <output png>   output image, always (indexed or truecolor) png with alpha
 <input>        input image(s) (png, jpg, gif, tga, bmp, pnm, psd)

CSV format:
 # comment
 width,height,srcx,srcy,dstx,dsty[,inputidx[,operators[,outsuffix]]]

Operators (more can be specified):
 a - use alpha blending instead of replace
 e - erase using alpha mask
 r - rotate clockwise
 c - rotate counter clockwise
 v - flip vertically
 h - flip horizontally
 g - scale, grow to double
```

### Installation

Just download, no dependencies, no install, portable executables.

- [sprsheet](https://gitlab.com/bztsrc/sprsheet/raw/main/sprsheet) (Linux, 317k)
- [sprsheet.exe](https://gitlab.com/bztsrc/sprsheet/raw/main/sprsheet.exe) (Windows, 349k)

### Compilation

Just run `make` in the src directory. No configure, no dependencies, suckless as it should be.

Specifying the Conversion
-------------------------

### Colors

If `-f` force given, then its parameter must be a PNG file with no more than 256 different colors. This option calculates
the sRGB distance and chooses the closest color on that palette, therefore always replaces all pixels, even the ones without
an exact match. (For example, make all similar browns a specific brown.)

If `-r` replace given, then its parameter must be a PNG file. Colors in the second row (or optionally more additional rows)
will be replaced by the colors in the first row. So the first row is the "to" palette, other rows are the "from" palettes.
(For example, make a certain gradient of green the same gradient of brown.)

If `-p` palette given, then its parameter must be a PNG file with the size of 8 x 16 pixels. This will be added to the bottom
right corner of the output image, and all colors matching the first row of this palette image replaced by grayscale colors
(color in first coloumn will become `#f0f0f0`, the one in the second `#d0d0d0`, the third `#b0b0b0` etc. and the color in the
last 8th coloumn `#101010`; see a figure about the other direction, recolorization
[here](https://codeberg.org/tirnanog/editor/raw/main/docs/img/recolor.png)).

All palette conversions preserve the original alpha channel.

### Layout

If the `<csv>` parameter is "-", then it assumes one input image, and one output with that input's size (dummy copy
without cropping).

The conversion is given in a CSV (comma separated values) file. Each row describes one rectangular area to copy. Normally
that means one rectangle per frame, but you can construct one frame from multiple rectangles if you wish, no restrictions.

```
width, height, source x, source y, dest x, dest y[, input file index[, operators[, output suffix]]]
```

The `width` and `height` specifies the size of the rectangle in pixels. The `source x` and `source y` are positions on the
input image, and `dest x` and `dest y` the position on the destination sheet. The last two coloumns are optional, if you specify
multiple input files, then the 7th is the `input file index` (if not given, then defaults to `0`, which is the first input file).
You can describe various operations on the selected area before you paste it on the destination sheet (like flipping, rotating,
apply alpha blending etc.) by concatenating the one-letter `operators` (defaults to simple copy without alpha blending).
If the 9th coloumn is given, then you can group certain rows with it, and also sepcifies the output filename's suffix.

There are several example data files in the repo:

 - [identity.csv](csv/identity.csv) can be used as a skeleton for your own conversion file
 - [merge.csv](csv/merge.csv) merge multiple input animation files into a single sheet
 - [split.csv](csv/split.csv) split a single input sheet into multiple animation files
 - [copy.csv](csv/copy.csv) dummy copy if you just want to use only the palette options (you can also use "-" as filename)
 - [lpc2tngbase.csv](csv/lpc2tngbase.csv) simple mapping between LPC and TirNanoG Base layout
 - [lpc2tngbase_male_body.csv](csv/lpc2tngbase_male_body.csv) fills in missing animations as much as possible, also fixes some annoying offset issues
 - [lpc2tngbase_male_body_portrait.csv](csv/lpc2tngbase_male_body_portrait.csv) same as lpc2tngbase_male_body, but also adds a portrait from a separate image
 - [lpc2tngbase_male_hands.csv](csv/lpc2tngbase_male_hands.csv) fixes offset issues to match hands position
 - [lpc2tngbase_female_body.csv](csv/lpc2tngbase_female_body.csv) same, for the female sprite sheets
 - [lpc2tngbase_female_head.csv](csv/lpc2tngbase_female_head.csv) fixes offset issues to match head position
 - [lpc2tngbase_female_dress.csv](csv/lpc2tngbase_female_dress.csv) fixes offset issues for dress-like assets
 - [lpc2tngbase_female_legs.csv](csv/lpc2tngbase_female_legs.csv) fixes offset issues for legwear assets
 - [lpc2tngbase_spider.csv](csv/lpc2tngbase_spider.csv) for idle(1)+attack(1+3)+walk(6) and die(4) type sheets

You can use any text editor to create these CSV files (or even Excel or gnumeric if that's your thing), or alternatively
you can also use [this web helper](https://bztsrc.gitlab.io/sprsheet) that runs in your browser (tested with Firefox).

Usage of the web helper:

1. drag'n'drop an example sprite sheet image into the canvas
2. press mouse button, move your mouse and release to select an area on the loaded sprite sheet (use <kbd>Shift</kbd> to snap to grid)
3. as soon as the selection is done, the sprite sheet disappears and the guidelines will appear
4. use the cursor arrow keys or drag'n'drop the selected box with the mouse to position it to fit the guideline
5. when the position is fine, press Enter or click on the "Next box" button (and go to 2.)
6. when you have finished with all the frames, press the "Save CSV" button.

After that the downloaded CSV file can be used with `sprsheet` as-is, and it will replay the same conversions you have
recorded on the web interface on any image file.

Output Image
------------

The resulting image is always saved as a PNG with alpha channel (no .so nor .dll needed, libpng included). Its size will be
automatically calculated from the given CSV, or you can explicitly set it with the `-s` size flag. It will be a truecolor
PNG (32 bit packed color for each pixel) if you add the `-t` flag, but by default it's going to be an indexed PNG (no more than
256 different colors allowed, but much much smaller and better compressed, one pixel takes only 8 bits, palette 24+8 bits).

If the 9th in the CSV is set, then there will be as many output images as many different values in that coloumn, each file
named as `<output png>_<suffix>.png`. In this case the size of each image is always determined by the rows referencing it.

Output is checked and only saved if it contains at least one non-transparent pixel. Empty images only saved if `-e` is given.

Input Images
------------

Input can be in any format that [stb_image](http://nothings.org/stb) understands:

- Portable Network Graphics (.png, 8-bit, 16-bit, grayscale, truecolor, indexed etc.)
- Photoshop Document (.psd, limited features, not all files supported)
- Joint Photographic Experts Group's (.jpg .jpeg, not all variants supported)
- Graphic Interchange Format (.gif, animations supported too)
- Targa Graphics (.tga)
- Windows Bitmap (.bmp, non-rle compressed only)

For animated gifs, the frames are extracted vertically (first frame on the top, last frame at the bottom).

TODO
----

There's no full LPC guideline exists yet, so most of the CSVs (as well as the guidelines.png that the web helper uses) are for
the [TirNanoG Base layout](https://tirnanog.codeberg.page/manual_en.html#sheet_layout). That's the layout that I'm
using anyway.

License
-------

Licensed under the terms of MIT license.


Cheers,

bzt
