/*
 * sprsheet/main.c
 *
 * Copyright (C) 2022 bzt
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Small utility to convert sprite sheets
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libpng/png.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_FAILURE_STRINGS
#include "stb_image.h"

/* command line arguments */
int width = 0, height = 0, indexed = 1, empty = 0;

typedef struct {
    int o, w, h, sx, sy, dx, dy, idx;
    char op[32];
} frame_t;
frame_t *frames = NULL;
int numframes = 0, maxidx = 0;
char *comment = NULL;

typedef struct {
    char name[256];
    int maxw, maxh;
    uint8_t *dst;
} out_t;
out_t *outs = NULL;
int numout = 0;

/**
 * Parse a CSV file
 */
int parse_csv(char *fn)
{
    FILE *f;
    char *ret = NULL, *ptr;
    unsigned int s = 0;
    int i, j, k, l = 1, m;

    if(!strcmp(fn, "-")) {
        i = numframes++;
        frames = (frame_t*)realloc(frames, numframes * sizeof(frame_t));
        if(!frames) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
        memset(&frames[i], 0, sizeof(frame_t));
        numout++;
        outs = (out_t*)realloc(outs, numout * sizeof(out_t));
        if(!outs) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
        memset(outs, 0, sizeof(out_t));
        maxidx = 0;
        return 1;
    }
    f = fopen(fn, "rb");
    if(f) {
        fseek(f, 0L, SEEK_END);
        s = (unsigned int)ftell(f);
        fseek(f, 0L, SEEK_SET);
        ret = (char*)malloc(s + 1);
        if(ret) {
            s = fread(ret, 1, s, f);
            ret[s] = 0;
        } else
            s = 0;
        fclose(f);
    }
    if(!s) { if(ret) free(ret); return 0; }
    for(ptr = ret; ptr < ret + s && *ptr; ptr++) {
        if(*ptr == ' ' || *ptr == '\t' || *ptr == '\r' || *ptr == '\n') {
            if(*ptr == '\n') l++;
            continue;
        }
        if(*ptr != '#') {
            k = 0;
            i = numframes++;
            frames = (frame_t*)realloc(frames, numframes * sizeof(frame_t));
            if(!frames) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            memset(&frames[i], 0, sizeof(frame_t));
            frames[i].w = atoi(ptr); while(*ptr >= '0' && *ptr <= '9') ptr++;
            if(*ptr == ',') {
                ptr++;
                frames[i].h = atoi(ptr); while(*ptr >= '0' && *ptr <= '9') ptr++;
                if(*ptr == ',') {
                    ptr++;
                    frames[i].sx = atoi(ptr); while(*ptr >= '0' && *ptr <= '9') ptr++;
                    if(*ptr == ',') {
                        ptr++;
                        frames[i].sy = atoi(ptr); while(*ptr >= '0' && *ptr <= '9') ptr++;
                        if(*ptr == ',') {
                            ptr++;
                            frames[i].dx = atoi(ptr); while(*ptr >= '0' && *ptr <= '9') ptr++;
                            if(*ptr == ',') {
                                ptr++;
                                frames[i].dy = atoi(ptr); while(*ptr >= '0' && *ptr <= '9') ptr++;
                                if(*ptr == ',') {
                                    ptr++;
                                    frames[i].idx = atoi(ptr);  while(*ptr >= '0' && *ptr <= '9') ptr++;
                                    if(*ptr == ',') {
                                        ptr++;
                                        for(j = 0; j < 31 && *ptr != '\r' && *ptr != '\n' && *ptr != ','; j++)
                                            switch(*ptr) {
                                                case 'a': case 'e': case 'r': case 'c': case 'v': case 'h': case 'g':
                                                    frames[i].op[j] = *ptr++;
                                                break;
                                                default:
                                                    fprintf(stderr,"Line %d: unknown operator '%c'\r\n", l, *ptr);
                                                    free(frames); frames = NULL;
                                                    free(ret);
                                                    return 0;
                                                break;
                                            }
                                        if(*ptr == ',') {
                                            ptr++;
                                            for(m = 0; ptr[m] && ptr[m] != '\r' && ptr[m] != '\n' && ptr[m] != ','; m++);
                                            for(k = 0; k < numout && ((int)strlen(outs[k].name) != m ||
                                                memcmp(ptr, outs[k].name, m)); k++);
                                            if(k >= numout) {
                                                k = numout++;
                                                outs = (out_t*)realloc(outs, numout * sizeof(out_t));
                                                if(!outs) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
                                                memset(&outs[k], 0, sizeof(out_t));
                                                memcpy(outs[k].name, ptr, m);
                                            }
                                            ptr += m;
                                            if(*ptr == ',') {
                                                ptr++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(frames[i].w < 1 || frames[i].h < 1 || frames[i].sx < 0 || frames[i].sy < 0 || frames[i].dx < 0 || frames[i].dy < 0) {
                fprintf(stderr,"Line %d: invalid dimension or negative numbers in coordinates\r\n", l);
                free(frames); frames = NULL;
                free(ret);
                return 0;
            }
            if(!outs || numout < 1) {
                k = numout++;
                outs = (out_t*)realloc(outs, numout * sizeof(out_t));
                if(!outs) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
                memset(&outs[k], 0, sizeof(out_t));
            }
            frames[i].o = k;
            if(outs[k].maxw < frames[i].dx + frames[i].w) outs[k].maxw = frames[i].dx + frames[i].w;
            if(outs[k].maxh < frames[i].dy + frames[i].h) outs[k].maxh = frames[i].dy + frames[i].h;
            if(maxidx < frames[i].idx) maxidx = frames[i].idx;
        }
        while(ptr < ret + s && *ptr && *ptr != '\r' && *ptr != '\n') ptr++;
        if(*ptr == '\n') l++;
    }
    for(i = 0; i < numout; i++) {
        outs[i].maxw = ((outs[i].maxw + 63) & ~63);
        outs[i].maxh = ((outs[i].maxh + 63) & ~63);
    }
    free(ret);
    return 1;
}

/**
 * Copy part of a pixelbuffer to another
 */
void copy_frame(uint8_t *src, int sw, int sh, frame_t *frame)
{
    uint8_t *s, *d, *a, *b, *b2, *dst;
    uint32_t *bp1, *bp2, tmp;
    char *o, alpha = 0;
    int i, j, w, h, width, height;

    if(!src || sw < 1 || sh < 1 || !frame || frame->o < 0 || frame->o >= numout || !outs[frame->o].dst) return;
    dst = outs[frame->o].dst;
    width = outs[frame->o].maxw;
    height = outs[frame->o].maxh;
    w = frame->w; h = frame->h;
    if(frame->sx + w > sw) w = sw - frame->sx;
    if(frame->sy + h > sh) h = sh - frame->sy;
    if(w < 1 || h < 1) return;
    if(frame->dx + w > width) w = width - frame->dx;
    if(frame->dy + h > height) h = height - frame->dy;
    if(w < 1 || h < 1) return;
    /* source to buffer */
    s = src + frame->sy * (sw * 4) + frame->sx * 4;
    d = b = (uint8_t*)malloc(w * h * 4);
    if(!b) { printf("\r\n"); fprintf(stderr,"Not enough memory\r\n"); exit(1); }
    for(i = 0; i < h; i++) {
        memcpy(d, s, w * 4);
        s += sw * 4; d += w * 4;
    }
    /* apply operators to buffer */
    for(o = frame->op; *o; o++)
        switch(*o) {
            case 'a': alpha = 1; break;
            case 'e': alpha = 2; break;
            case 'v':
                for(j = 0; j < h; j++) {
                    bp1 = (uint32_t*)(b + j * w * 4);
                    for(i = 0; i < w / 2; i++) {
                        tmp = bp1[i];
                        bp1[i] = bp1[w - 1 - i];
                        bp1[w - 1 - i] = tmp;
                    }
                }
            break;
            case 'h':
                for(j = 0; j < h / 2; j++) {
                    bp1 = (uint32_t*)(b + j * w * 4);
                    bp2 = (uint32_t*)(b + (h - 1 - j) * w * 4);
                    for(i = 0; i < w; i++) {
                        tmp = bp1[i];
                        bp1[i] = bp2[i];
                        bp2[i] = tmp;
                    }
                }
            break;
            case 'c':
            case 'r':
                b2 = (uint8_t*)malloc(w * h * 4);
                if(!b2) { printf("\r\n"); fprintf(stderr,"Not enough memory\r\n"); exit(1); }
                bp1 = (uint32_t*)b;
                bp2 = (uint32_t*)b2;
                for(j = 0; j < h; j++)
                    for(i = 0; i < w; i++)
                        bp2[*o == 'r' ? (h - 1 - j) + i * h : (w - 1 - i) * h + j] = bp1[j * h + i];
                free(b);
                b = b2;
                i = w;
                w = h;
                h = i;
            break;
            case 'g':
                b2 = (uint8_t*)malloc(w * 2 * h * 2 * 4);
                if(!b2) { printf("\r\n"); fprintf(stderr,"Not enough memory\r\n"); exit(1); }
                bp1 = (uint32_t*)b;
                bp2 = (uint32_t*)b2;
                for(j = 0; j < h; j++)
                    for(i = 0; i < w; i++) {
                        bp2[j * 4 * w + 2 * i] = bp2[j * 4 * w + 2 * i + 1] =
                        bp2[(j * 2 + 1) * 2 * w + 2 * i] = bp2[(j * 2 + 1) * 2 * w + 2 * i + 1] = bp1[j * w + i];
                    }
                free(b);
                b = b2;
                w <<= 1;
                h <<= 1;
            break;
        }

    /* buffer to dest */
    s = b2 = b;
    d = dst + frame->dy * (width * 4) + frame->dx * 4;
    switch(alpha) {
        case 2:
            for(j = 0; j < h; j++) {
                for(i = 0, a = d, b = s; i < w; i++, a += 4, b += 4) {
                    if(b[3]) a[3] = a[2] = a[1] = a[0] = 0;
                }
                s += w * 4; d += width * 4;
            }
        break;
        case 1:
            for(j = 0; j < h; j++) {
                for(i = 0, a = d, b = s; i < w; i++, a += 4, b += 4) {
                    if(!a[3]) {
                        a[3] = b[3];
                        a[2] = b[2];
                        a[1] = b[1];
                        a[0] = b[0];
                    } else
                    if(b[3]) {
                        a[3] = (b[3]*b[3] + (256 - b[3])*a[3]) >> 8;
                        a[2] = (b[2]*b[3] + (256 - b[3])*a[2]) >> 8;
                        a[1] = (b[1]*b[3] + (256 - b[3])*a[1]) >> 8;
                        a[0] = (b[0]*b[3] + (256 - b[3])*a[0]) >> 8;
                    }
                }
                s += w * 4; d += width * 4;
            }
        break;
        default:
            for(i = 0; i < h; i++) {
                memcpy(d, s, w * 4);
                s += w * 4; d += width * 4;
            }
        break;
    }
    free(b2);
}

/**
 * Add a unique color to palette from image
 */
int cpaladd(uint8_t *pal, int r, int g, int b, int a, int *mc)
{
    int i, dr, dg, db, da, m, q;
    int64_t d, dm;

    for(q=1; q<=8; q++) {
        m=-1; dm=3*65536*256+1;
        for(i=0; i<*mc; i++) {
            if(r==pal[i*4+0] && g==pal[i*4+1] && b==pal[i*4+2] && (a == -1 || a==pal[i*4+3])) return i;
            if(r>>q==pal[i*4+0]>>q && g>>q==pal[i*4+1]>>q && b>>q==pal[i*4+2]>>q && (a == -1 || a>>q==pal[i*4+3]>>q)) {
                db = b - pal[i*4+2]; dg = g - pal[i*4+1]; dr = r - pal[i*4+0]; da = a - pal[i*4+3];
                d = dr*dr + dg*dg + db*db + (a == -1 ? 0 : da);
                if(d < dm) { dm = d; m = i; }
                if(!dm) break;
            }
        }
        if((a == -1 || dm>9+9+9+9) && i<256) {
            pal[i*4+3] = a;
            pal[i*4+2] = b;
            pal[i*4+1] = g;
            pal[i*4+0] = r;
            (*mc)++;
            return i;
        }
        if(m>=0) {
            pal[m*4+3] = ((pal[m*4+3] + a) >> 1);
            pal[m*4+2] = ((pal[m*4+2] + b) >> 1);
            pal[m*4+1] = ((pal[m*4+1] + g) >> 1);
            pal[m*4+0] = ((pal[m*4+0] + r) >> 1);
            return m;
        }
    }
    /* should never reached */
    return 0;
}

/**
 * Load an image
 */
unsigned char *image_load(char *fn, int *w, int *h)
{
    FILE *f;
    stbi__context s;
    stbi__result_info ri;
    unsigned char *data = NULL;
    char *com;
    int c = 0, nf = 1;

    *w = *h = 0;
    f = stbi__fopen(fn, "rb");
    if(!f) return NULL;
    stbi__start_file(&s, f);
    if(stbi__gif_test(&s)) {
        data = stbi__load_gif_main(&s, NULL, w, h, &nf, &c, 4);
        if(data && *w > 0 && *h > 0 && nf > 1)
            *h *= nf;
    } else {
        data = stbi__load_main(&s, w, h, &c, 4, &ri, 8);
    }
    fclose(f);
    com = stbi_comment();
    if(data && *w > 0 && *h > 0) {
        if(com) {
            if(!comment) comment = com;
            else free(com);
        }
        return data;
    }
    if(data) free(data);
    if(com) free(com);
    return NULL;
}

/**
 * Write image to file
 */
int image_save(uint8_t *p, int w, int h, char *fn)
{
    FILE *f;
    uint8_t *data = NULL, pal[1024] = { 0 };
    png_color ppal[256];
    png_byte trns[256];
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep *rows = (png_bytep*)malloc(h * sizeof(png_bytep));
    png_text texts[1] = { 0 };
    int i, t, mc = 0;

    if(!empty) {
        for(i = 0; i < w * h && p[i * 4 + 3] < 3; i++);
        if(i >= w * h) { if(rows) free(rows); return 0; }
    }
    if(!rows) { printf("\r\n"); fprintf(stderr,"Not enough memory\r\n"); exit(1); }
    data = (uint8_t*)malloc(w * h);
    if(!data) { free(rows); printf("\r\n"); fprintf(stderr,"Not enough memory\r\n"); exit(1); }
    f = fopen(fn, "wb+");
    if(!f) { free(rows); free(data); printf("\r\n"); fprintf(stderr,"Unable to write %s\r\n", fn); exit(2); }
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) { free(rows); free(data); fclose(f); return 0; }
    info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) { png_destroy_write_struct(&png_ptr, NULL); free(rows); free(data); fclose(f); return 0; }
    if(setjmp(png_jmpbuf(png_ptr))) { png_destroy_write_struct(&png_ptr, &info_ptr); free(rows); free(data); fclose(f); return 0; }
    png_init_io(png_ptr, f);
    png_set_compression_level(png_ptr, 9);
    png_set_compression_mem_level(png_ptr, 5);
    png_set_filter(png_ptr, PNG_FILTER_TYPE_BASE, PNG_FILTER_VALUE_NONE);
    if(indexed) {
        t = PNG_COLOR_TYPE_PALETTE;
        for(i = 0; i < w * h; i++)
            data[i] = cpaladd(pal, p[i * 4], p[i * 4 + 1], p[i * 4 + 2], p[i * 4 + 3], &mc);
        for(i = 0; i < h; i++) rows[i] = data + i * w;
        for(i = 0; i < mc; i++) {
            ppal[i].red = pal[i*4+0];
            ppal[i].green = pal[i*4+1];
            ppal[i].blue = pal[i*4+2];
            trns[i] = pal[i*4+3];
        }
        png_set_PLTE(png_ptr, info_ptr, ppal, mc);
        png_set_tRNS(png_ptr, info_ptr, trns, mc, NULL);
    } else {
        t = PNG_COLOR_TYPE_RGB_ALPHA;
        for(i = 0; i < h; i++) rows[i] = p + i * w * 4;
    }
    png_set_IHDR(png_ptr, info_ptr, w, h, 8, t, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    if(comment && *comment) {
        texts[0].key = "Comment"; texts[0].text = comment;
        png_set_text(png_ptr, info_ptr, texts, 1);
    }
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(rows);
    free(data);
    fclose(f);
    return 1;
}

/**
 * Usage instructions
 */
void usage()
{
    printf("sprsheet by bzt Copyright (C) 2022 MIT license\r\n https://gitlab.com/bztsrc/sprsheet\r\n\r\n");
    printf("./sprsheet [-s <w>,<h>] [-f <png>] [-r <png>] [-p <png>] [-t] <csv>|- <output png> <input #1> [input #2] [input #3...]\r\n\r\n");
    printf(" -s <w>,<h>     specify the output image's size (otherwise calculated from CSV)\r\n");
    printf(" -f <png>       force colors to be the ones on the parameter image\r\n");
    printf(" -r <png>       replace colors using image rows (first row \"to\", others \"from\")\r\n");
    printf(" -p <png>       define palette variations and de-colorize\r\n");
    printf(" -t             make the output a truecolor image\r\n");
    printf(" -e             save output even if it's empty\r\n");
    printf(" <csv>          CSV file that describes the areas to be used, one row per frame\r\n");
    printf(" <output png>   output image, always (indexed or truecolor) png with alpha\r\n");
    printf(" <input>        input image(s) (png, jpg, gif, tga, bmp, pnm, psd)\r\n\r\n");
    printf("CSV format:\r\n # comment\r\n width,height,srcx,srcy,dstx,dsty[,inputidx[,operators[,outsuffix]]]\r\n\r\n");
    printf("Operators (more can be specified):\r\n");
    printf(" a - use alpha blending instead of replace\r\n");
    printf(" e - erase using alpha mask\r\n");
    printf(" r - rotate clockwise\r\n");
    printf(" c - rotate counter clockwise\r\n");
    printf(" v - flip vertically\r\n");
    printf(" h - flip horizontally\r\n");
    printf(" g - scale, grow to double\r\n\r\n");
    exit(0);
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
    uint8_t *p = NULL, *pal = NULL, *repl = NULL, *s, *d, fix[1024] = { 0 };
    char *csv = NULL, *out = NULL, fn[1024];
    int i, j, k, o, l = -1, c, w, h, rw, rh, tot, cur, mc = 0, dr, dg, db;
    int64_t di, dmi;

    /* parse command line */
    for(i = 1; i < argc && !out; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 's': sscanf(argv[++i], "%d,%d", &width, &height); break;
                case 'r':
                    repl = image_load(argv[++i], &rw, &rh);
                    if(!repl || rw < 1 || rh < 2) { fprintf(stderr,"Unable to load replacement palette %s\r\n", argv[i]); exit(2); }
                break;
                case 'f':
                    p = image_load(argv[++i], &w, &h);
                    if(!p || w < 1 || h < 1) { fprintf(stderr,"Unable to load forced palette %s\r\n", argv[i]); exit(2); }
                    for(j = 0; j < w * h * 4 && mc < 256; j += 4)
                        cpaladd(fix, p[j], p[j + 1], p[j + 2], -1, &mc);
                    free(p);
                break;
                case 'p':
                    pal = image_load(argv[++i], &w, &h);
                    if(!pal || w != 8 || h != 16) { fprintf(stderr,"Unable to load variant palette %s\r\n", argv[i]); exit(2); }
                break;
                case 't': indexed = 0; break;
                case 'e': empty = 1; break;
                default: fprintf(stderr,"Unknown flag '%c'\r\n", argv[i][1]); exit(2);
            }
        } else {
            if(!csv) csv = argv[i];
            else out = argv[i];
        }
    if(!csv || !out || i >= argc) usage();
    if(!parse_csv(csv) || !outs || numout < 1 || !frames || numframes < 1) { fprintf(stderr,"Unable to load %s\r\n", csv); exit(2); }
    if(!width || !height) {
        if(!outs[0].maxw || !outs[0].maxh) {
            p = image_load(argv[i], &outs[0].maxw, &outs[0].maxh);
            if(numframes) {
                frames[0].w = outs[0].maxw;
                frames[0].h = outs[0].maxh;
            }
            if(p) free(p);
        }
        width = outs[0].maxw; height = outs[0].maxh;
    } else {
        outs[0].maxw = width; outs[0].maxh = height;
    }
    if(width < 64 || height < 64 || width > 16384 || height > 16384) {
        fprintf(stderr,"Unable to determine output image dimensions\r\n"); exit(2);
    }
    if(maxidx > argc - i) { fprintf(stderr,"Not enough input images, %d required for this CSV\r\n", maxidx + 1); exit(2); }

    for(o = 0; o < numout; o++) {
        outs[o].dst = (uint8_t*)malloc(outs[o].maxw * outs[o].maxh * 4);
        if(!outs[o].dst) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
        memset(outs[o].dst, 0, outs[o].maxw * outs[o].maxh * 4);
    }
    /* add frames from source images to output */
    tot = numframes * (argc - i);
    for(k = cur = dr = 0; i < argc; i++, k++) {
        /* load input */
        p = image_load(argv[i], &w, &h);
        if(!p) { printf("\r\n"); fprintf(stderr,"Unable to load %s\r\n", argv[i]); dr++; continue; }
        for(j = 0; j < numframes; j++) {
            c = (cur++ * 100) / tot;
            if(c != l) { l = c; printf("\r%s... %d%%", out, c); fflush(stdout); }
            if(k == frames[j].idx)
                copy_frame(p, w, h, &frames[j]);
        }
    }
    free(frames);
    if(dr == tot / numframes) {
        printf("\r%s... FAILED\n", out);
        exit(2);
    }

    for(o = 0; o < numout; o++) {
        if(outs[o].name[0]) sprintf(fn, "%s_%s.png", out, outs[o].name); else strcpy(fn, out);

        /* force all colors to match a specified fix palette */
        printf("\r%s... palettes", fn); fflush(stdout);
        if(mc) {
            for(d = outs[o].dst, j = 0; j < outs[o].maxh; j++)
                for(k = 0; k < outs[o].maxw; k++, d += 4) {
                    for(s = fix, i = l = 0, dmi = 3*65536*256+1; i < mc && dmi; i++, s += 4) {
                        dr = d[0] - s[0]; dg = d[1] - s[1]; db = d[2] - s[2];
                        di = dr*dr + dg*dg + db*db;
                        if(di < dmi) { dmi = di; l = i; }
                    }
                    d[0] = fix[l*4+0]; d[1] = fix[l*4+1]; d[2] = fix[l*4+2];
                }
        }
        /* replace colors according to rows in an image file */
        if(repl) {
            for(d = outs[o].dst, i = 0; i < outs[o].maxw * outs[o].maxh; i++, d += 4)
                for(s = repl + rw * 4, j = 0; j < (rh - 1); j++)
                    for(k = 0; k < rw; k++, s += 4)
                        if(repl[k * 4 + 3] == 0xff && s[3] == 0xff && d[0] == s[0] && d[1] == s[1] && d[2] == s[2]) {
                            d[0] = repl[k * 4]; d[1] = repl[k * 4 + 1]; d[2] = repl[k * 4 + 2];
                            j = rh; break;
                        }
        }
        /* color variations */
        if(pal) {
            if(outs[o].maxw > 8 && outs[o].maxh > 16) {
                /* de-colorize */
                for(d = outs[o].dst, i = 0; i < outs[o].maxw * outs[o].maxh; i++, d += 4)
                    for(j = k = 0; j < 8; j++, k += 4)
                        if(pal[k + 3] == 0xff && d[0] == pal[k] && d[1] == pal[k + 1] && d[2] == pal[k + 2]) {
                            d[0] = d[1] = d[2] = ((7 - j) << 5) | 0x10;
                            break;
                        }
                /* copy palette with variations to the bottom right corner */
                s = pal; d = outs[o].dst + (outs[o].maxh - 16) * (outs[o].maxw * 4) + (outs[o].maxw - 8) * 4;
                for(i = 0; i < 16; i++) {
                    memcpy(d, s, 8 * 4);
                    s += 8 * 4; d += outs[o].maxw * 4;
                }
            }
        }

        /* save result image */
        printf("\r%s... saving  ", fn); fflush(stdout);
        image_save(outs[o].dst, outs[o].maxw, outs[o].maxh, fn);
        printf("\r%s... OK      \r\n", fn);
        free(outs[o].dst);
    }
    if(outs) free(outs);
    if(repl) free(repl);
    if(pal) free(pal);
    if(comment) free(comment);
    return 0;
}
